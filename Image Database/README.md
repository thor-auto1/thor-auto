This directory contains our earthquake-site image database with semantic semgentation annotations.

<b>Thor Database Hierarchy </b><br>
|── <b>2_column_all_image_labels.txt </b> # the whole image database in 2-column format (left column: image filename ; right column: label filename) <br>
|── <b>all_labels_480x360_grayscale</b> # all the labels from the image database resized to 480x360 pixels and in single-channel grayscale format <br>
|── <b>train </b># images and labels for training FCN models <br>
 &emsp;&emsp;├── <b>images_480x360 </b># all training images resized to 480x360 pixels <br>
 &emsp;&emsp;├── <b>images_original_size </b># all training images without resizing <br>
 &emsp;&emsp;├── <b>labels_480x360_grayscale </b># all training labels resized to 480x360 pixels and in single-channel grayscale format<br>
 &emsp;&emsp;├── <b>labels_original_size_RGB </b># all training labels without resizing and in three-channel RGB format <br>
 &emsp;&emsp;├── <b>2_column_train.txt </b># all the training image-labels in 2-column format (left column: image filename ; right column: label filename) <br>
|── <b>val </b># images and labels for testing and validating FCN models <br>
 &emsp;&emsp;├── <b>images_480x360 </b># all validation images resized to 480x360 pixels <br>
 &emsp;&emsp;├── <b>images_original_size </b># all validation images without resizing <br>
 &emsp;&emsp;├── <b>labels_480x360_grayscale </b># all validation labels resized to 480x360 pixels and in single-channel grayscale format<br>
 &emsp;&emsp;├── <b>labels_original_size_RGB </b># all validation labels without resizing and in three-channel RGB format <br>
 &emsp;&emsp;├── <b>2_column_val.txt </b># all the validation image-labels in 2-column format (left column: image filename ; right column: label filename) <br>

A few images and their corresponding annotations are shown below: <br>
![annotated_images_from_database](/uploads/f3aa1d1dd4025e169098f31093ef2721/annotated_images_from_database.PNG)

<br>
The pixel annotation tool used in this project was created by: <br>
https://github.com/abreheret/PixelAnnotationTool <br>
<br>
The Windows x64 version of this annotation tool along with the relevant config file for this project can be downloaded from the below link: <br> 
https://gitlab.com/thor-auto1/thor-auto/-/raw/master/Image_Database/PixelAnnotationTool_x64_v1.3.2_for_Thor_Database.zip<br>
<br>
Images may need to be resized to be used with this annotation tool or for training FCN models. <br>
If so, an image resizing tool for Windows can be downloaded and installed from here: <br>
https://gitlab.com/thor-auto1/thor-auto/-/raw/master/Image_Database/ImageResizerSetup-3.1.1.exe<br>
which was created by: <br>
https://github.com/bricelam/ImageResizer <br>
<br>

<b>RGB Label Correspondance: </b> <br>
void 		0 	0 	0	  --> black <br>
building	70 	70 	70	--> gray <br>
vehicle		0 	0 	142	--> blue <br>
cracks		243 121 0	--> orange <br>
other		170 85 	127	--> dark pink <br>
person		220 20 	60	--> red <br>
road		128 64 	128	--> purple <br>
sky			70 	130 180	--> steel blue <br>
vegetation	107 142 35	--> light green <br>
water		85 	255 255	--> light blue <br>


<b>Grayscale Label Correspondance: </b> <br>
void 		--> 0 <br>
building	--> 1 <br>
vehicle		--> 2 <br>
cracks		--> 3 <br>
other		--> 4 <br>
person		--> 5 <br>
road		--> 6 <br>
sky			--> 7 <br>
vegetation	--> 8 <br>
water		--> 9 <br>
