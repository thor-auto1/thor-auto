#!/bin/sh
# install_Thor_part_1.sh

echo ' install_Thor_part_1.sh    '
echo '*** This script is to be ran after Jetpack 4.5.1 is installed, from /home/nvidia/Desktop/ ***'
echo '*** And also please do the following before running this script: ***'
echo 'Install nomachine if remote access is used (see remote_access_instructions.txt)'
echo 'Install SD card or other storage device and format it as ext4, unmount it if already mounted (use gparted or see jetson_setup_instructions.txt)'

echo 'set eth1 (lidar) to static ipv4 in the gui, this must be in the same subnet as laser'
echo " --> 192.168.13.14 255.255.255.0 192.168.13.1"

echo 'Edit this script below, such as /dev/mmcblk2p1 or /dev/nvme0n1 with the external storage path' 
echo 'Copy misc_files folder to the Desktop'
echo 'Comment out the two lines in this script that mention EOF'
echo '****************************************************************************'
read -p "Press Enter to continue" x
echo 'Installing Software for Thor...'
cd /home/nvidia/Desktop/

#: <<'EOF'
#EOF

apt-get install -y gparted
sed -i -e '$a\' /etc/fstab

mkdir /home/nvidia/Desktop/ext_storage
sed -i -e '$a\' /etc/fstab
echo '/dev/nvme0n1	/home/nvidia/Desktop/ext_storage	ext4	defaults	0	0' >> /etc/fstab
mount /home/nvidia/Desktop/ext_storage
chmod 777 /home/nvidia/Desktop/ext_storage

# Move files from Desktop to ext_storage
mv misc_files/ /home/nvidia/Desktop/ext_storage/misc_files

# ERFNet Caffe (non-verbose) prep
sudo apt-get install -y libprotobuf-dev libleveldb-dev libsnappy-dev libopencv-dev libboost-all-dev libhdf5-serial-dev libgflags-dev libgoogle-glog-dev liblmdb-dev protobuf-compiler libatlas-base-dev
sudo apt-get install -y python-dev python-pip gfortran
cd /home/nvidia/Desktop/ext_storage/
sudo rm -rf ERFNet-Caffe/
git clone --recursive https://github.com/Yuelong-Yu/ERFNet-Caffe.git
chmod 777 ERFNet-Caffe
chmod 777 -R ERFNet-Caffe/*
cp /home/nvidia/Desktop/ext_storage/misc_files/argmax_layer.cu /home/nvidia/Desktop/ext_storage/ERFNet-Caffe/caffe-erfnet/src/caffe/layers/argmax_layer.cu
cp /home/nvidia/Desktop/ext_storage/misc_files/argmax_layer.hpp /home/nvidia/Desktop/ext_storage/ERFNet-Caffe/caffe-erfnet/include/caffe/layers/argmax_layer.hpp
cp /home/nvidia/Desktop/ext_storage/misc_files/net.cpp /home/nvidia/Desktop/ext_storage/ERFNet-Caffe/caffe-erfnet/src/caffe/net.cpp
cp /home/nvidia/Desktop/ext_storage/misc_files/requirements.txt /home/nvidia/Desktop/ext_storage/ERFNet-Caffe/caffe-erfnet/python/requirements.txt
cp /home/nvidia/Desktop/ext_storage/misc_files/Makefile.config /home/nvidia/Desktop/ext_storage/ERFNet-Caffe/caffe-erfnet/Makefile.config


sed -i -e '$a\' ~/.bashrc
echo 'PYTHONPATH=/home/nvidia/Desktop/ext_storage/ERFNet-Caffe/caffe-erfnet/python:$PYTHONPATH' >> ~/.bashrc

source ~/.bashrc
# MAY NEED TO MANUALLY EXECUTE THE ABOVE LINE
cd /home/nvidia/Desktop/ext_storage/ERFNet-Caffe/caffe-erfnet/python/
for req in $(cat requirements.txt); do sudo -H pip install $req; done
### this may take ~51min on TX2; 12:21am - 12:43am AGX
### there may be red errors throughout the process, but as long as the automated installation
### keeps going, then the errors can be ignored.


echo '*** PART 2 ***'

apt-get install -y python-skimage
apt update -y

export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/lib/aarch64-linux-gnu/pkgconfig/
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/lib/aarch64-linux-gnu/pkgconfig

# OpenCV 3.3.1
# somewhere it asks to replace opencv files with the new one, select A
cp /home/nvidia/Desktop/ext_storage/misc_files/opencv-3.3.1.zip /home/nvidia/Desktop/ext_storage/opencv-3.3.1.zip
cd /home/nvidia/Desktop/ext_storage/
sudo rm -rf opencv-3.3.1.zip/
sudo rm -rf opencv-3.3.1/
unzip opencv-3.3.1.zip
sudo apt-get install -y libjpeg8-dev libtiff5-dev libjasper-dev libpng12-dev
sudo apt-get install -y libgtk2.0-dev
sudo apt-get install -y libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
sudo apt-get install -y libatlas-base-dev gfortran
sudo apt-get install -y libhdf5-serial-dev
sudo apt-get install -y python2.7-dev
cd /home/nvidia/Desktop/ext_storage/opencv-3.3.1
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D WITH_CUDA=ON \
    -D ENABLE_FAST_MATH=1 \
    -D CUDA_FAST_MATH=1 \
    -D WITH_CUBLAS=1 \
    -D INSTALL_PYTHON_EXAMPLES=ON \
    -D BUILD_EXAMPLES=ON ..

cd /home/nvidia/Desktop/ext_storage/opencv-3.3.1/build/
make -j$(nproc)
sudo make install
sudo ldconfig

# ERFNet-Caffe Install
cd /home/nvidia/Desktop/ext_storage/ERFNet-Caffe/caffe-erfnet
make all -j$(nproc) && make pycaffe && make pytest && make test && make runtest
protoc src/caffe/proto/caffe.proto --cpp_out=.
mkdir include/caffe/proto
mv src/caffe/proto/caffe.pb.h include/caffe/proto

# ROS Melodic
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
sudo apt update -y
sudo apt-get install -y ros-melodic-desktop-full
sudo apt install -y python-rosdep python-rosinstall python-rosinstall-generator python-wstool build-essential
sudo rosdep init
rosdep update -y
echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
source ~/.bashrc
# MAY NEED TO MANUALLY EXECUTE THE ABOVE LINE
sudo apt install -y libjpeg-turbo8-dev libjpeg8-dev libturbojpeg0-dev python-rosinstall python-rosinstall-generator python-wstool build-essential ros-melodic-image-transport ros-melodic-image-publisher ros-melodic-vision-msgs hostapd dnsmasq
sudo apt-get install -y ros-melodic-bfl ros-melodic-urg-node
sudo apt-get install -y ros-melodic-navigation

# ZED Camera
read -p "Please Install the ZED Camera Manually (press enter to see the steps)" x
cd /home/nvidia/Desktop/ext_storage/misc_files/
chmod 777 ZED_SDK_Tegra_JP45_v3.5.0.run
echo '*** Please do these manually ***'
echo "./ZED_SDK_Tegra_JP45_v3.5.0.run"
echo " --> yes static version"
echo " --> yes object detection module"
echo " --> yes max perf mode"
echo " --> yes samples"
echo " --> yes auto install dependencies libjpeg-turbo8 libturbojpeg libusb-1.0 libopenblas-dev libv4l-0 curl unzip libpng16-16 libpng-dev libturbojpeg0-dev qt5-default libqt5opengl5 libqt5svg5 "
echo " --> yes install python API"
echo " --> python executable: python3"
echo "note: --> errors may show up but installation should still complete"
echo "After the ZED SDK is done installing, continue onto install_Thor_part_2.sh"
su nvidia


echo Done!
