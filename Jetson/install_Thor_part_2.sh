#!/bin/sh
# install_Thor_part_2.sh

echo ' install_Thor_part_2.sh    '
echo '*** This script is to be ran after install_Thor_part_1.sh is ran***'
echo '*** And also please do the following before running this script: ***'
echo 'Note: this script will do some of the setup for programming the teensy, for the remainder, see teensy_programming_from_jetson_instructions.txt'
echo 'Note: while the Jetson-Inference suite is installing, uncheck all models and skip pytorch'

read -p "Press Enter to continue" x

#: <<'EOF'
#EOF

# Jetson-Inference
cd /home/nvidia/Desktop/ext_storage/
sudo rm -rf jetson-inference/
git clone --recursive https://github.com/dusty-nv/jetson-inference
cd jetson-inference/
mkdir build
cd build
cmake ../
echo "Unchecked all models and skip pytorch"
make -j$(nproc)
sudo make install
sudo ldconfig
cd ..
cd ..

# Set up IP for Lidar interface (in addition to the GUI)
sed -i -e '$a\' /etc/network/interfaces
echo '# Setup eth1 (Hokuyo)' >>/etc/network/interfaces
echo 'auto eth1' >>/etc/network/interfaces
echo 'iface eth1 inet static' >>/etc/network/interfaces
echo 'address 192.168.13.14' >>/etc/network/interfaces
echo 'netmask 255.255.255.0' >>/etc/network/interfaces
echo 'metric 2000' >>/etc/network/interfaces
echo '# Internet network interface' >>/etc/network/interfaces
echo 'allow-hotplug eth0' >>/etc/network/interfaces
echo 'iface eth0 inet dhcp' >>/etc/network/interfaces

# Wifi hotspot (may not be needed if GUI is used)
echo "Setting up wifi hotspot..."
sed -i -e '$a\' /etc/modprobe.d/bcmdhd.conf
echo 'options bcmdhd op_mode=2' >> /etc/modprobe.d/bcmdhd.conf
sudo apt-get install -y hostapd
sudo apt-get install -y dnsmasq
sudo systemctl start dnsmasq
sudo systemctl start hostapd

# Thor-ROS
cd /home/nvidia/Desktop/ext_storage/misc_files/
mv Thor-ROS.zip /home/nvidia/Desktop/ext_storage/Thor-ROS.zip
cd /home/nvidia/Desktop/ext_storage/
unzip Thor-ROS.zip
echo "Time to build Thor-ROS !"

apt install ros-melodic-urg-node

cd /home/nvidia/Desktop/ext_storage/Thor-ROS/
sudo rm -rf build/
sudo rm -rf devel/

# ZED wrapper (from: https://github.com/stereolabs/zed-ros-wrapper#build-the-program)
cd /home/nvidia/Desktop/ext_storage/Thor-ROS/src
sudo rm -rf zed-ros-wrapper/
git clone -b v3.5 https://github.com/stereolabs/zed-ros-wrapper.git
chmod -R 777 zed-ros-wrapper/
cd ../
rosdep update -y
rosdep install --from-paths src --ignore-src -r -y
catkin_make -DCMAKE_BUILD_TYPE=Release

cd /home/nvidia/Desktop/ext_storage/Thor-ROS/src
sudo rm -rf zed-ros-examples/
git clone -b v3.5 https://github.com/stereolabs/zed-ros-examples.git
chmod -R 777 zed-ros-examples
cd ..
rosdep install --from-paths src --ignore-src -r -y
catkin_make -DCMAKE_BUILD_TYPE=Release

cd /home/nvidia/Desktop/ext_storage/Thor-ROS
sh compile_and_build.sh

# Teensy build
echo "Setting up to also update and flash the teensy board..."
echo "See teensy_programming_from_jetson_instructions_verX.txt for additional instructions for programming the Teensy "
apt-get install -y ros-melodic-rosserial-arduino
apt-get install -y ros-melodic-rosserial
cd /home/nvidia/Desktop/ext_storage/misc_files
rm -rf arduino-1.8.13/
tar -xvf arduino-1.8.13-linuxaarch64.tar.xz
cd arduino-1.8.13/
sh install.sh
sudo usermod -a -G dialout nvidia
cd /home/nvidia/Desktop/ext_storage/misc_files
chmod 777 TeensyduinoInstall.linuxaarch64
./TeensyduinoInstall.linuxaarch64 --dir=arduino-1.8.13/
cd arduino-1.8.13/hardware/teensy/avr/cores/teensy3
make

cd /home/nvidia/Desktop/ext_storage/misc_files
unzip TinyGPSPlus-1.0.2b.zip
chmod 777 -R TinyGPSPlus-1.0.2b/
mv TinyGPSPlus-1.0.2b /home/nvidia/Desktop/ext_storage/misc_files/arduino-1.8.13/libraries/TinyGPSPlus
apt-get -y remove --purge modemmanager
sudo cp 00-teensy.rules /etc/udev/rules.d/ 
cd /home/nvidia/Desktop/ext_storage/Thor-ROS/teensy_board
chmod 777 teensy_board.ino 

# may need to do these manually
cd /home/nvidia/Desktop/ext_storage/Thor-ROS
source devel/setup.bash
cd ..
cd /home/nvidia/Desktop/ext_storage/misc_files/arduino-1.8.13/libraries/
rm -rf ros_lib
rosrun rosserial_arduino make_libraries.py .
cd /home/nvidia/Desktop/ext_storage/Thor-ROS
echo "If teensy doesnt work, manually enter the 7 lines above this one or go through teensy_programming_from_jetson.txt to get the teensy flashed"
echo "To setup the wifi hotspot, see: https://www.youtube.com/watch?v=GAOvGAdwiHk"
echo "Name of wifi video by JetsonHacks: Wi-Fi Hotspot Setup - NVIDIA Jetson Developer Kits "
echo "Reboot to make sure everything is setup correctly."
echo "Then you can run the ROS program that controls the car platform by doing this:"
echo " --> source devel/setup.bash"
echo " --> roslaunch start_car_everything.launch"
echo "Done!"


