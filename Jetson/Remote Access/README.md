# Remote Access Setup

The Jetson doesn't need a monitor connected in order to interact with it and it's GUI.<br>
This directory contains instructions for setting up this remote access to the Jetson. <br>
<br>
<b> Before Proceeding </b> <br>
Make sure both your user system and the Jetson are on the same network. <br>
<br>
<b>If your user system is a Windows machine: </b> <br>
Here are the steps after testing from a Windows 10 machine: <br>
1) Download nomachine from [https://www.nomachine.com/download/download&id=116&s=ARM](https://www.nomachine.com/download/download&id=116&s=ARM) <br>
*This will be installed on the Jetson <br>
2) Copy the downloaded file to the Jetson's Desktop folder <br>
3) Install nomachine on the Jetson using these commands: <br>
```
sudo tar zxvf nomachine_7.7.4_1_aarch64.tar.gz
sudo NX/nxserver --install
```
4) Download Install nomachine on Windows from:
[https://www.nomachine.com/download](https://www.nomachine.com/download) <br>
The instructions should be straight forward from that installer. <br>
5) From the jetson board: <br>
From the top left menu, type "User Accounts" and click the first icon that shows up <br>
6) Click Unlock on the top-right side of the window <br>
7) Click Automatic Login so that it is set to ON <br>
8) Click Lock on the top-right-side of the window (then close the window) <br>
9) From the top left menu, type "Brightness & Lock" and click the first icon that shows up <br>
10) Configure the settings as below: <br>
```
Turn screen off when inactive for: Never
Lock: OFF 
Lock screen after: Screen turns off
```
<br>
<b>If your user system is an Ubuntu machine: </b> <br>
Here are the steps after testing from an x86 Ubuntu 18 machine: <br> <br>

1) From the Jetson: <br>
   Replace the org.gnome.Vino.gschema.xml file in /usr/share/glib-2.0/schemas/ <br>
   with the [org.gnome.Vino.gschema.xml file linked in this directory](https://gitlab.com/thor-auto1/thor-auto/-/raw/master/Jetson/Remote%20Access/org.gnome.Vino.gschema.xml). <br>
   For example, the following command will accomplish this by overwriting the original file:
   <br>
   ```
	cp org.gnome.Vino.gschema.xml /usr/share/glib-2.0/schemas/org.gnome.Vino.gschema.xml
   ```
   <br>
2) Enter the below command (no errors should be reported):
   <br>
   ```
	sudo glib-compile-schemas /usr/share/glib-2.0/schemas
   ```
3) From the top left menu, type "Desktop Sharing" and click the first icon that shows up <br>
4) Configure the settings as below:
   <br>
   ```
   Check: Allow other users to view your desktop
   Check: Allow other users to control your desktop
   Uncheck: You must confirm each access to this machine
   Check: Require the user to enter this password (and then enter your password in the text box)
   Uncheck: Automatically configure UPnP router to open and forward ports
   ```
5) From the top left menu, type "Startup Applications" and click the first icon that shows up <br>
6) Click Add and in the new window that pops up, fill in the fields as below (then click Add): <br>
   ```
   Name: Vino
   Command: /usr/lib/vino/vino-server
   ```
7) Close the Startup Applications Preference Window

8) Open a terminal and enter the below commands:
   ```
   gsettings set org.gnome.Vino require-encryption false
   gsettings set org.gnome.Vino prompt-enabled false
   ```
   Note: a warning may pop up

9) From the remote machine accessing the jetson board: <br>
    Type "Remmina" in the Activities search bar and click the first icon that shows up (should be blue)
	
10) Click the green plus sign

11) Enter a name for the Profile (such as TX2 or jetson board)

12) Change the Protocol setting to "VNC - Virtual Network Computing"

13) For Server: <br>
	Enter the IP address of the Jetson board <br>
	The IP can be found by using ifconfig (CLI) or viewing the Connection Information from the top right mini-icon in the Desktop (GUI) <br>
	
14) For User name and password:
	Enter the username when using the jetson board (non-root) along with the password

15) Change Color depth and Quality to adjust how well and fast the jetson GUI looks when accessing it remotely
	My settings are set as True color (32 bpp) and Best (slowest), however, you may find settings that work better for you

16) Click Save

17) From the jetson board: <br>
	From the top left menu, type "User Accounts" and click the first icon that shows up
	
18) Click Unlock on the top-right side of the window

19) Click Automatic Login so that it is set to ON

20) Click Lock on the top-right-side of the window (then close the window)

21) From the top left menu, type "Brightness & Lock" and click the first icon that shows up

22) Configure the settings as below:
```
Turn screen off when inactive for: Never
Lock: OFF
Lock screen after: Screen turns off
```
23) Reboot the Jetson board and connect to it using Remmina Remote Desktop
<br>
<br>
<b> After the remote access is confirmed working </b> <br>
You can remove the HDMI cable and monitor from the Jetson and replace it with an "HDMI Dummy Plug". <br>
The purpose of the HDMI dummy plug is to enable the GUI to be used on the Jetson, while accessing it remotely and without a direct keyboard, video, mouse connected to the Jetson board. <br>
The HDMI Dummy Plug that we successfully used for this project came from Amazon: <br>
[https://www.amazon.com/gp/product/B07FB4VJL9](https://www.amazon.com/gp/product/B07FB4VJL9)
