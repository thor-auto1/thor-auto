# Jetson Computing Platform

The Jetson needs specific software installed in order to run with Thor-ROS. <br>
This directory contains the files and instructions for loading the Jetson with compatibile software. <br>
<br>
<b> Installing JetPack </b> <br>
The first step is to image the Jetson with a JetPack. <br>
The software to run the Thor platform has been tested and built to work with JetPack 4.5.1, which includes the following: <br>
```
Jetson OS 32.5.1 
CUDA 10.2
OpenCV 4.1.1
cuDNN 8.0
TensorRT 7.1.3
```

*Other JetPack versions may also function, but those may require additional steps and for software to be reconfigured.
<br>
<br>
While installing JetPack and prompted for additional information, fill out that information as shown below: <br>
```
  Your name: nvidia
  Computer's name: nvidia 
  Username: nvidia 
  Password: nvidia 
  Performance Mode: MAXN
```
For more detailed instructions for installing JetPack, see the user guide in the box that came with the Jetson or the below link may also help: <br>
[https://docs.nvidia.com/jetson/jetpack/install-jetpack/index.html#how-to-install-jetpack](https://docs.nvidia.com/jetson/jetpack/install-jetpack/index.html#how-to-install-jetpack)<br>
<br>
<b>Note</b>:<br>
We recommend to install [NVIDIA's SDK Manager](https://developer.nvidia.com/nvidia-sdk-manager) on a separate x86 Ubuntu machine and then use it as a host system for installing the JetPack on the target Jetson board. <br>  
<br>
<b> After the Jetson is Imaged with JetPack </b> <br>
Please do the following: <br>
- Install "nomachine" for remote access and for the ability to interact with the Jetson Desktop GUI without a monitor connected to the Jetson board.<br>
  See [Remote Access Setup](https://gitlab.com/thor-auto1/thor-auto/-/tree/master/Jetson/Remote%20Access) for these detailed instructions. <br>
- Install an SD card or other storage media to the Jetson, and configure it as follows: <br>
  - Formatted as ext4 <br>
  - Make sure it is an unmounted device (gparted software can help with this) if you are using the automated script described below while installing the other Jetson software. <br>
- Set the Lidar IP as a static IP by using the GUI in Ubuntu. <br>
  The IPs and subnet masks should be entered like this: <br>
  192.168.13.14 255.255.255.0 192.168.13.1 <br>
- If you plan on using the automated script, edit the line (in [install_Thor_part_1.sh](https://gitlab.com/thor-auto1/thor-auto/-/raw/master/Jetson/install_Thor_part_1.sh)) referencing the "fstab" file with the path to your external storage media.<br>
  So instead of /dev/nvme0n1, it should say something like /dev/mmcblk2p1 or some other disk partition according to your media and what's connected to the Jetson. <br>
- Download, Unzip, and Copy the [misc_files](https://www.dropbox.com/s/9l3cz5ysm6cnlso/misc_files.zip?dl=0) folder and the two "install_Thor.sh" scripts to the Jetson desktop, if you are using those two automated scripts. <br> 
  After the unzip, move all those files such that the following directory and exact tree structure exists: <br>
  /home/nvidia/Desktop/misc_files/models
<br>

<b> Running the automated scripts </b> <br>
We made a couple of automated scripts to speed up the time and reduce the effort for installing and configuring all of the other software to work with the Thor. <br> 
Once the steps mentioned above are complete, begin the automated software process by running and following the [install_Thor_part_1.sh](https://gitlab.com/thor-auto1/thor-auto/-/raw/master/Jetson/install_Thor_part_1.sh) script.<br>

```
root@nvidia:/home/nvidia/Desktop # sh install_Thor_part_1.sh
``` 

The script may take a few hours to finish executing (at least one or two hours). <br>
<br>
Once complete, it will prompt you to install the ZED SDK manually.<br>
The ZED SDK can be installed by running the below command as a non-root user: <br>
```
./ZED_SDK_Tegra_JP45_v3.5.0.run
```
When the ZED installer asks you which features to install, select "yes" to all of those features. <br>
And select the python executable as "python3" <br>
<br>
Afterwards, run and follow the [install_Thor_part_2.sh](https://gitlab.com/thor-auto1/thor-auto/-/raw/master/Jetson/install_Thor_part_2.sh) script. <br>

```
root@nvidia:/home/nvidia/Desktop # sh install_Thor_part_2.sh
``` 

While this script installs the Jetson-Inference package from [https://github.com/dusty-nv/jetson-inference](https://github.com/dusty-nv/jetson-inference), it will ask for some user interaction. <br>
At this stage, the listed models can be unchecked and pytorch can also be skipped. <br>
<br>

<b> Wifi Hotspot Setup </b> <br>
The purpose of setting up a hotspot on the Jetson is to enable remote access for when there is no network for you and the Jetson to both be connected to. <br>
The most common use case for this will be when the Jetson is taken outside for field tests. <br>
<br>
Please follow the instructions from the following youtube video to setup the Wifi Hotspot: <br>
[https://www.youtube.com/watch?v=GAOvGAdwiHk](https://www.youtube.com/watch?v=GAOvGAdwiHk) <br>
<br>

<b> Teensy Setup </b> <br>
The automated scripts above setup some of the Teensy programming, but not all of it. <br>
To setup the remainder of the Teensy programming, see the instructions at this link: <br>
[Teensy Setup Instructions](https://gitlab.com/thor-auto1/thor-auto/-/tree/master/Teensy%20Setup) <br>

Note: <br>
if you see the following error message while trying to run the car platform, please try reflashing the Teensy board by using the instructions at the above link: <br>

```
  Unable to sync with device; possible link problem or link software version mismatch such as hydro rosserial_python with groovy Arduino
```

<b> Running the car platform </b> <br>
Once everything is setup and working on the car platform, make sure you are logged in as root and in the /home/nvidia/Desktop/ext_storage/Thor-ROS/ directory. <br>
Then the following commands can be used to run the ROS program that starts the self-driving: <br>

```
source devel/setup.bash
roslaunch start_car_everything.launch
```

<b> Recompiling code on the Jetson </b> <br>
When changing any code on the Jetson, enter this command to recompile the ROS program: <br>

```
sh compile_and_build.sh
```

