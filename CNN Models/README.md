# CNN Models

This directory contains all the CNN models we tested along with their weights which were deployed to the Jetson platforms.

All models were trained with CUDA 11.0, cuDNN 7.6, and Caffe-Thor.
