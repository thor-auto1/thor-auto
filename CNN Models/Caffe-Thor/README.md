# Caffe-Thor <br>

This directory contains the modified Caffe used in Thor, and is based on the following pre-existing Caffe versions: <br>

https://github.com/BVLC/caffe <br>

https://github.com/Yuelong-Yu/ERFNet-Caffe/tree/master/caffe-erfnet <br>


