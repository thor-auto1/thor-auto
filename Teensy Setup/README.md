# Teensy Setup Instructions

The Teensy board needs to be setup in order to be programmed and controlled by the Jetson board. <br>
<br>
Please do the following to finish up setting up the Jetson for the Teensy: <br>
Enter these commands on the Jetson, as root: <br>

```
cd /home/nvidia/Desktop/ext_storage/Thor-ROS
source devel/setup.bash
cd ..
cd /home/nvidia/Desktop/ext_storage/misc_files/arduino-1.8.13/libraries/
rm -rf ros_lib
rosrun rosserial_arduino make_libraries.py .
cd /home/nvidia/Desktop/ext_storage/Thor-ROS
```

Open the Arduino IDE (search for it in the Jetson searchbar in the top left of the screen) <br>
Configure the IDE as follows: <br>

```
Board: Teensy 3.2/3.1
Tools: Port: "/dev/ttyACM0 (Teensy)"
Tools: Get Board Info
```

The expected output for the board info should be similar to the below:

```
BN: Teensy
VID: 16C0
PID: 0483
SN: 7994580 or 4474620
```

<br>
<b> If you are adding or changing the ROS message details or format between the Teensy and Jetson boards </b> <br>
Here are some useful steps to avoid context related errors: <br>
<br>
- Update the code on both the Jetson board and the Teensy board by considering the new and relevant messages in the #include files <br>
- Add a new .msg file in the src/autobot/msg/ directory <br>
- Update the Thor-ROS/src/autobot/CMakeLists.txt with the new message name. <br>
- Do a clean rebuild of the ROS program:

```
cd /home/nvidia/Desktop/ext_storage/Thor-ROS
rm -rf build/
rm -rf devel/
sh compile_and_build.sh
```

Then enter the following commands:

```
source devel/setup.bash
cd ..
cd misc_files/arduino-1.8.13/libraries
rm -rf ros_lib
rosrun rosserial_arduino make_libraries.py .
```

Note: The teensy software and the Arduino ide have to be compatible software versions <br>

Then you can flash the teensy using the Arduino IDE <br>
<br>

# Special PCB for the Teensy and other Car components
The Teensy board goes into a special custom PCB that helps connect the rest of the car components to the Jetson, such as the Motor, Servo, RPM Sensor, GPS and Compass. <br>
<br>
Unfortunately, all the EAGLE PCB files (schematic and board files) were lost from my computer, so I'm unable to share those. <br>
<br>
However, I was able to recover the Gerber files from the vendor who originally fabricated these PCBs. <br>
<br>
The PCB vendor used was [JLCPCB](https://jlcpcb.com/) and the Gerber files they saved for me can be downloaded from this link: <br>
[https://gitlab.com/thor-auto1/thor-auto/-/blob/master/Teensy%20Setup/MOTOR_PCB_REV_F.zip](https://gitlab.com/thor-auto1/thor-auto/-/blob/master/Teensy%20Setup/MOTOR_PCB_REV_F.zip) <br>
<br>
If you ever need additional PCBs for the car motor and such, please send those files linked above to a PCB vendor such as the one linked above.<br>
<br>
They may charge a small fee.
