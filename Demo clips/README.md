# Demo clip videos <br>

This directory contains a few short demo clips of the vehicle prototype performing self driving. <br>

There isn't exactly a disaster impacted area such as an earthquake in our local community, so instead we had to test the car platform at an alternate location that was available to us. <br>

The same general piece of code is implemented for all videos shown below, with only some slight modification of parameters based on the camera's posture at the time of testing. <br>

<b> Local navigation (Both CNN and Lidar)</b> <br>
The following demos clips emphasize and verify the vehicles navigating based on our sensor fusion of both camera and lidar sensor data. <br>
Note: in the following clips, road crack detection is disabled due to the large number of false-positives that were detected from adjacent blocks within each sidewalk. <br>
For validating the navigation with the road crack detection, the road surface would typically have to be smooth and consistent, such as on regularly maintained full-sized roads, that don't consist of all the tiny cracks that exist on sidewalks and walkways. <br>
<br>
TX2:<br>
- Link to demo clip:<br>
https://www.dropbox.com/s/6r6hpg730scg8zn/TX2_400.mp4?dl=0 <br>
- This demo clip is the same as the above, except here the speed of the vehicle is slightly increased when certain conditions are met: <br>
https://www.dropbox.com/s/zhqj07u3ddr9zat/TX2_500.mp4?dl=0 <br>
<br>

AGX:<br>
- Link to demo clip:<br>
https://www.dropbox.com/s/9wm0vgex9pkrrro/AGX_400.mp4?dl=0 <br>
- This demo clip is the same as the above, except here the speed of the vehicle is slightly increased when certain conditions are met: <br>
https://www.dropbox.com/s/0p62fi6j5zufzxl/AGX_500.mp4?dl=0 <br>
<br>

<b> Local navigation (CNN Only)</b> <br>
The following short clips show the platform navigating while ignoring Lidar data, thus using only the CNN vision processing for this test: <br>
Note: road crack detection is enabled for this test
- Link to demo clip: <br>
https://www.dropbox.com/s/8jpz1k07htrlr86/AGX_300_CNN_only.mp4?dl=0 <br>
- This test is the same as the above, except the speed of the vehicle is slightly increased when allowed: <br>
https://www.dropbox.com/s/2ej1xtbf37gw9wc/AGX_400_CNN_only.mp4?dl=0 <br>
- Same test, but with the speed increased once more, when allowed: <br>
https://www.dropbox.com/s/4zydt8ygiszpx0h/AGX_500_CNN_only.mp4?dl=0 <br>

<br>
<b> Global navigation </b> <br>
The following clip emphasizes and verify the vehicle navigating based on the GPS and Compass data, while still considering camera and lidar data.<br>
The platform's task is to reach a destination point from a starting point, through a means of intermediate checkpoints. <br>
When the vehicle is within 7.5 meters away from a checkpoint, then it considers the checkpoint as reached, then it will proceed on to the next checkpoint. <br>
Once all checkpoints are reached, then the vehicle stops moving. <br>
<br>
Link to demo clip: <br>
https://www.dropbox.com/s/zbsb925939x536j/Global_navigation_demo_clip.mov?dl=0
<br>
<br>

<b>Increasing the speed further</b> <br>
The car is capable of driving at increased speeds than shown in the above videos, however, navigation errors are amplified at increased speeds, which introduces risk of collision. <br>
In future work, a few ways that these errors can be minimized is to: <br>
- Optimize sensor synchronization between Camera/Lidar/Encoder code. <br>
- Stabilize the base of the car platform to reduce wobble and improve posture of the sensors. <br>
- Additional calibrations or upgrades to the GPS/Compass modules. <br>
- Upgrade the Encoder as the Traxxas supplied RPM sensor is known to not have best-in-class accuracy. <br>
- Using faster CNN models or implement quantization.<br>
- Additional optimization of the navigation decision based on the CNN output mask.
