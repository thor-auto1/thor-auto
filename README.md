# Thor: A Mobile Platform Towards Self-Navigation in Disaster-Struck Zones <br>

In a disaster-struck zone, such as from an earthquake, roads are often severely damaged and dangerous to drive through. <br>
To autonomously navigate through such conditions, small self-driving vehicles are advantageous and can detect the commonly found objects in earthquake-struck zones. <br>
We call this developed platform Thor, and it can detect road cracks, water puddles, humans, amoungst several other types of objects which can be found in the disaster impacted areas. <br>
Thor autonomously detects these objects using a Fully Convolutional Neural Network (FCN) model which uses semantic segmentation to classify objects at a pixel-level. <br>
Various FCN models were trained and tested with the developed earthquake-site image database presented in this repository. <br>
The training and testing of those models can help us understand their impact towards accuracy, performance, and energy efficiency for mobile self-driving platforms. <br>

<b> Project Authors </b> <br>
Ryan Zelek <br>
Dr. Hyeran Jeon (Advisor) <br>

## Publication
The platforms and image database used in this project were built for the work done in our paper published on IEEE Access: <br>

<!--**"Characterization of Semantic Segmentation Models on Mobile Platforms for Self-Navigation in Disaster-Struck Zones"**, R. Zelek, H. Jeon, arXiv:2202.01421 [cs], Feb. 2022. [[pdf]](https://arxiv.org/pdf/2202.01421)  <br> -->
**"Characterization of Semantic Segmentation Models on Mobile Platforms for Self-Navigation in Disaster-Struck Zones"**, <br>
R. Zelek and H. Jeon, in IEEE Access, vol. 10, pp. 73388-73402, 2022, doi: 10.1109/ACCESS.2022.3190014. [[pdf]](https://ieeexplore.ieee.org/stamp/stamp.jsp?tp=&arnumber=9826736)

<br>

Please cite this paper if you found our results or database useful for your research. <br>

## Getting Started
Once all the hardware and boards are powered on and physically connected, follow the instruction from the following link to setup and use the code: <br>
[https://gitlab.com/thor-auto1/thor-auto/-/tree/master/Jetson](https://gitlab.com/thor-auto1/thor-auto/-/tree/master/Jetson) <br>

## Acknowledgement
This project is built based on the former team's project: <br> <br>
[`https://github.com/Doyal88/Self_Driving_car_earthquake`](https://github.com/Doyal88/Self_Driving_car_earthquake)




